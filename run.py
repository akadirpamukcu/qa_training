from run_qg import run_qg

args_dict = {
    "model_name_or_path": "google/mt5-small",
    "model_type": "t5",
    "tokenizer_name_or_path": "t5_qg_tokenizer",
    "output_dir": "multi_t5-qa",
    "train_file_path": "data/train_data_qa_qg_hl_t5.pt",
    "valid_file_path": "data/valid_data_qg_hl_t5.pt",
    "per_device_train_batch_size": 32,
    "per_device_eval_batch_size": 32,
    "gradient_accumulation_steps": 8,
    "learning_rate": 1e-4,
    "num_train_epochs": 3,
    "seed": 31,
    "do_train": True,
    "do_eval": True,
    "evaluate_during_training": True,
    "save_steps": 2000,
    "logging_steps": 100,
    "overwrite_output_dir": True
}

# start training
run_qg(args_dict)